#include <iostream>
using namespace std;
void zer (int wart, int &ref);
void nzer (int wart, int ref);
int main () {
    int a = 44, b = 77;
    cout << "Before nzer function:\n";
    cout << "a = " << a << ", b = " << b << endl;
    nzer (a,b);
    cout << "After the nzer function:\n";
    cout << "a = " << a << ", b = " << b << endl;
    cout << "Before calling zer function:\n";
    cout << "a = " << a << ", b = " << b << endl;
    zer (a,b);
    cout << "After zer function:\n";
    cout << "a = " << a << ", b = " << b << endl;
    return 0;
}
void zer (int wart, int &ref) {
    cout << "\tIn zer before release: \n";
    cout << "\twart = " << wart << ", ref = " << ref << endl;
    wart = 0;
    ref = 0;
    cout << "\tIn zer function after zeros placed: \n";
    cout << "\twart = " << wart << ", ref = " << ref << endl;
}
void nzer (int wart, int ref) {
    cout << "\tIn nzer before release: \n";
    cout << "\twart = " << wart << ", ref = " << ref << endl;
    wart = 0;
    ref = 0;
    cout << "\tIn nzer function after zeros placed: \n";
    cout << "\twart = " << wart << ", ref = " << ref << endl;
}