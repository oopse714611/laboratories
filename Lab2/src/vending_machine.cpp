#include <iostream>
#include <string>
using namespace std;

int disp_vend(int a, int b, int c, int d);
int validate_check(int secret_code,int get_num);
int quant_check(int prod,int quant);


int main(){
    int initQuan = 10;
    int quant;
    int prod1 = initQuan, prod2 = initQuan, prod3 = initQuan, prod4 = initQuan , get_num; 
    int secret_code = 1111;
    while (1){
	disp_vend (prod1, prod2, prod3, prod4); 
	cout << "Which product you want to pick?";
	cout << "(Enter the product number): ";
	cin >> get_num;
	
	if (validate_check(secret_code, get_num) == 0) break;
	
	cout << "Pick quantity: "; 
	cin >> quant;
	
	switch (get_num)
	{
	    case 1:
	    if (quant_check(prod1, quant) < 0){ cout << "No such quantity in the storage!"; continue;}; 
	    prod1 = quant_check(prod1, quant);
	
	    break;
	    case 2:
	    if (quant_check(prod2, quant) < 0){ cout << "No such quantity in the storage!"; continue;}; 
	    prod2 = quant_check(prod2, quant);
	    break;
	    case 3:
	    if (quant_check(prod3, quant) < 0){ cout << "No such quantity in the storage!"; continue;};
	    prod3 = quant_check(prod3, quant);
	    break;
	    case 4:
	    if (quant_check(prod4, quant) < 0){ cout << "No such quantity in the storage!"; continue;};
	    prod4 = quant_check(prod4, quant);
	    break;
	    default: cout << "Value different than 1,2,3,4" << endl;
	}
    }
    cout << "Exit" << endl;
    return 0; 
}

int disp_vend(int a, int  b, int c, int d){

    cout << "no | product | quantity" << endl;
    cout << "1  | cola    | "<< a << endl;
    cout << "2  | chips   | "<< b << endl;
    cout << "3  | carrot  | "<< c << endl;
    cout << "4  | water   | "<< d << endl;
    return 0; 
}

int validate_check(int secret_code,int get_num){
    int valid = 1;
    if (secret_code == get_num) valid = 0;
    return valid;
}

int quant_check(int prod, int quant){
    return (prod - quant);
}