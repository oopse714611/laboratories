#include"functions.hpp"

#include<iostream>
#include<limits>

using namespace std;

void dispMenu(){
    cout << "What do you want to do?\n";
    cout << "1. Load hero.\n";
    cout << "2. Make new hero.\n";
    cout << "3. Save hero.\n";
    cout << "4. Edit hero.\n";
    cout << "5. Generate new monsters.\n";
    cout << "6. Save monster set.\n";
    cout << "7. Load monster team.\n";
    cout << "8. Exit game.\n";

}

int user_action(){
    dispMenu();
    return get_num_answer(1,8);
}

int get_num_answer(int min, int max){
    int number;

    while (true){
        cout << "Choose number between " << min << " and " << max << ": ";
        if (cin >> number){
            if (number >= min && number <= max){
                return number;
            }else{
                cout << "Wront input\n";
                continue;
            }
        }else{
            cout << "Wrong input!\n";
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            continue;
        }
    }

    return number;
}


void use_profession(character & h, mage m, warrior w, berserker b, thief t){
    switch(h.get_prof_num()){
        case 1:
            m.enchant(h);
            break;
         case 2:
            w.enchant(h);
            break;
        case 3:
            b.enchant(h);
            break;
        case 4:
            t.enchant(h);
            break;
        default:
            break;
    }
}

void cancel_profession(character & h, mage m, warrior w, berserker b, thief t){
    switch(h.get_prof_num()){
        case 1:
            m.disenchant(h);
            break;
         case 2:
            w.disenchant(h);
            break;
        case 3:
            b.disenchant(h);
            break;
        case 4:
            t.disenchant(h);
            break;
        default:
            break;
    }    
}

