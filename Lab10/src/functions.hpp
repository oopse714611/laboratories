#include<iostream>
#include<string>

#include"class.hpp"

#ifndef functions_HPP
#define functions_HPP

void dispMenu();
int user_action();
int get_num_answer(int min, int max);

void use_profession(character & h, mage m, warrior w, berserker b, thief t);
void cancel_profession(character & h, mage m, warrior w, berserker b, thief t);

#endif