#include<iostream>
#include<string>
#include<fstream>
#include <limits>
#include <cstdlib>
#include <cstdio>
#include <ctime>

using namespace std;

#include"class.hpp"
#include"functions.hpp"

int main(){
    srand( time( NULL) );
    


    character hero;
    mage m;
    warrior w;
    berserker b;
    thief t;

    monster monsterek[5];



    
    string answer;
    int exit = 1;

    //the game 
    while(exit){
        for (int i = 0; i<5 ; i++){
            monsterek[i].display_full();
        }
        hero.display_full();
        switch(user_action()){
        case 1:
            //Load hero
            cout << "What is your hero's name? : ";
            cin >> answer;
            hero.load(answer);
            use_profession(hero, m, w, b, t);
            break;
        case 2:
            //Create a new hero
            hero.set_to_default();
            cout << "New neme: ";
            cin >> answer;
            hero.change_name(answer);
            hero.edit();
            hero.set_profession();
            use_profession(hero, m, w, b, t);
            break;
        case 3:
            //Save hero
            cancel_profession(hero, m, w, b, t);
            hero.save_character();
            use_profession(hero, m, w, b, t);
            cout << "Your hero has been saved\n";
            break;
        case 4: 
            //Edit hero
            hero.edit();
            hero.set_profession();
            use_profession(hero, m, w, b, t);
            break;
        case 5:
            //Generate new monsters
            for(int i = 0; i<5; i++){
                monsterek[i].generate();
                monsterek[i].change_name("monster_" + to_string(i + 1));
            }
            break;
        case 6:
            //save monster set
            cout << "Set monster team's name: ";
            cin >> answer;
            for (int i = 0; i<5; i++){
                monsterek[i].change_name(monsterek[i].name + "_" + answer);
                monsterek[i].save_character();
            }
            break;
        case 7:
            //load monster team
            cout << "Give monster team's name: ";
            cin >> answer;
            for (int i = 1; i < 6; i++){
                monsterek[i-1].load("monster_" + to_string(i) + "_" + answer);
            }
            break;
        case 8:
            //Exit
            cout << "Do you want to save your hero? [y/n]: ";
            if (cin >> answer){
                exit = 0;
                if (answer == "y") {cancel_profession(hero, m, w, b, t); hero.save_character(); cout << "Thank you for playing. Your hero has been saved.\n";}
                else if (answer == "n") cout << "Thank you for playing. Your hero has not been saved!\n";
                else {cout << "No exit for you! Try one more time.\n"; exit = 1;}
            }
            break;
        default:
            cout << "Error! Try other option.\n";
            break;
        } 
    }


    return 0;
}