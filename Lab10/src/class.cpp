#include"class.hpp"
#include"functions.hpp"

#include<iostream>
#include<string>
#include<fstream>

using namespace std;

character::character(){
    set_to_default();
}

character::character(std::string Name){
    set_to_default();
    change_name(Name);
}

character::character(std::string Name, int Strength, int Dexterity, int Endurance, int Intelligence, int Charisma){
    name = Name;
    strength = Strength;
    dexterity = Dexterity;
    endurance = Endurance;
    intelligence = Intelligence;
    charisma = Charisma;
}

void character::set_to_default(){
    name = "Unknown";
    strength = 2;
    dexterity = 2;
    endurance = 2;
    intelligence = 2;
    charisma = 2;
    profession = "Polaczek";
}

void character::edit(){
    cout << "Strength\n";
    edit_Strength(get_num_answer(0, default_value));
    cout << "Dexterity\n";
    edit_Dexterity(get_num_answer(0, default_value));
    cout << "Endurance\n";
    edit_Endurance(get_num_answer(0, default_value));
    cout << "Intelligence\n";
    edit_Intelligence(get_num_answer(0, default_value));
    cout << "Charisma\n";
    edit_Charisma(get_num_answer(0, default_value));
}

void character::change_name(std::string new_name){name = new_name;}
void character::edit_Strength(int v){strength = v;}
void character::edit_Dexterity(int v){dexterity = v;}
void character::edit_Endurance(int v){endurance = v;}
void character::edit_Intelligence(int v){intelligence = v;}
void character::edit_Charisma(int v){charisma = v;}


void character::load(std::string address){
    if (address != "Unknown"){
        std::string full_address = "./" + address + ".txt";
        fstream file;
        file.open(full_address, ios::in);
        if (file.fail()) cout << "Error during openning file: " << "address\n";
        else{
            file >> name >> profession >> prof_num >> strength >> dexterity >> endurance >> intelligence >> charisma;
        }
        file.close();
    }else cout << "Cannot load default heros!\n";
}


void character::display_full(){
    cout << "======================";
    cout << "\nName: " << name << "\nProfession: " << profession << "\nStrength: " << strength << "\nDexterity: " << dexterity << "\nEndurance: " << endurance << "\nIntelligence: " << intelligence << "\nCharisma: " << charisma; 
    cout << "\n======================\n";
}

void character::save_character(){
    if (name != "Unknown"){
        string full_address = "./" + name + ".txt";
        fstream file;
        file.open(full_address, ios::out | ios::trunc);
        if (file.fail()) cout << "Error saving the character\n";
        else{
            file << name << " " << profession << " " << prof_num << " " << strength << " " << dexterity << " " << endurance << " " << intelligence << " " << charisma;
            cout << "Character saved.\n";
        }
        file.close();
    }else cout << "Cannot save default heroes!\n";
}


void character::set_profession(){

    cout << "Select the hero class:\n";
    cout << "1.Mage\n2.Warrior\n3.Berserker\n4.Thief\n";
    cout << "Type number to select a class: ";
    prof_num = get_num_answer(1,4);

    switch(prof_num){
        case 1:
            profession = "Mage";
            break;
        case 2:
            profession = "Warrior";
            break;
        case 3:
            profession = "Berserker";
            break;
        case 4:
            profession = "Thief";
            break;
        default:
            profession = "Polaczek";
            break;
    }
}

void character::set_profession(std::string prof, int num){
    profession = prof;
    prof_num = num;
}

int character::get_prof_num(){
    return prof_num;
}

void mage::enchant(character & h){
    h.edit_Intelligence(h.intelligence + 5);
}
void mage::disenchant(character & h){
    h.edit_Intelligence(h.intelligence - 5);
}

void warrior::enchant(character & h){
    h.edit_Endurance(h.endurance + 5);
}
void warrior::disenchant(character & h){
    h.edit_Endurance(h.endurance - 5);
}

void berserker::enchant(character & h){
    h.edit_Strength(h.strength + 5);
}
void berserker::disenchant(character & h){
    h.edit_Strength(h.strength - 5);
}

void thief::enchant(character & h){
    h.edit_Dexterity(h.dexterity + 5);
}
void thief::disenchant(character & h){
    h.edit_Dexterity(h.dexterity - 5);
}

int monster::total = 0;

monster::monster(){
    monster::total++;
    change_name("monster_" + to_string(monster::total));

    generate();
    set_profession("monster", 5);
}

void monster::generate(){
    int ans;
    ans = rand() % 10 +  1;
    edit_Intelligence(ans);
    ans = rand() % 10 +  1;
    edit_Charisma(ans);
    ans = rand() % 10 +  1;
    edit_Dexterity(ans);
    ans = rand() % 10 +  1;
    edit_Endurance(ans);
    ans = rand() % 10 +  1;
    edit_Strength(ans);
}