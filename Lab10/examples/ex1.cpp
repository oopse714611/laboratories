#include<iostream>

using namespace std;

//Class declarations ==============

class door;

class home {
    private: 
        bool flowerHydr;
    public:
        home (bool v) {flowerHydr = v;}

        void dayPassed(){
            flowerHydr = 0;
            cout << "Day has passed. flower needs water\n";
        }

        void ownerHydFlower(){
            flowerHydr = 1;
            cout << "Flower has been hydrated by the house owner\n";
        }

        friend void gardener(home& h, door& d);
};

class door {
    private: 
        bool lock;
    public:
        door(bool v) {lock = v;}

        void openDoor(){
            lock = 0;
            cout << "Door has been opened\n";
        }

        void closeDoor(){
            lock = 1;
            cout << "Door has been closed\n";
        }

        friend void gardener(home& h, door& d);
};

void gardener(home & h, door & d){
    d.openDoor();
    h.flowerHydr = 1;
    cout << "Flowe has been hydrated by the gardener\n";
    d.closeDoor();
}

int main(){

    home h1(0);
    door d1(1);

    h1.ownerHydFlower();
    cout << "Owner: it is time for ...\n";
    d1.closeDoor();
    h1.dayPassed();
    gardener(h1,d1);

    return 0;
}