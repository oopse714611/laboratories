#include<iostream>

using namespace std;

class hero{
    private:
        int str, dex, vit, in, cha;
    public:
        void przyp_alt(int x){
            str = 1*x; dex = 2*x; vit = 3*x; in = 5*x; cha = 4*x;
        }

        void wysw_art(){
            cout << "\thero status:";
            cout << "\n\tStrength: " << str;
            cout << "\n\tDexterity: " << dex;
            cout << "\n\tInteligence: " << in;
            cout << "\n\tVitality: " << vit;
            cout << "\n\tCharisma: " << cha << endl;
        }
};

class knight : public hero {
    public:
        knight (int l){
            przyp_alt(l);
            wysw_art();
        }
};

int main(){
    int b;
    do {
        cout << "Your knight is being created. Enter a number from 0 to 9: ";
        cin >> b;
        if (b > 9 || b < 0){
            cout << "Lear to read! The number supposed to be from 0 to 9\n";
        }
    } while (b > 9 || b < 0);
    
    knight r1(b);
    
    return 0;
}