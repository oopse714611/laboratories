#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
#include <cmath>

using namespace std; 

string access_file(string adress);
int calculate_change(float needed_money, float nominal[6], int *wallet, int *insert_cash, int *change, int decimal_multiplier);
void display_change(int change[6], float change_sum, float nominal[6]);
void display_cash(int cash[6], float nominal[6]);
string new_cash_record(string next_r, int cash[6]);

int number_of_products(string data, string next_l);
void display_products(int row, int col, string products[][4], int max_space);
int validate_check(int secret_code_1, int secret_code_2,int get_num, int num_of_products);
int continue_check(void);
void payment(int *insert_cash, float nominal[6]);
void collect_coins(int *insert_cash, int  *cash);
float coin_counter(int *insert_cash, float nominal[6]);
void empty_collector (int *insert_cash);
void money_edit(float nominal[6], int *cash);

void update_file(string address, string text);
string product_string(int row, int col, string products[][4]);
string cash_string(int cash[6]);



int main(){
    //--PARAMETERS-DEFINITION---------------------
    
    //conversion characters
    string next_c = ",";
    string next_r = "|";

    //get file 
    string data;
    int n;

    //CASH CONVERTERS
    string cash_s[6]; //each space for current number of specific nominal monets: 5pln 2pln 1pln 0.5pln 0.2pln 0.1pln
    float nominal[6];


    nominal[0] = 5;
    nominal[1] = 2;
    nominal[2] = 1;
    nominal[3] = 0.5;
    nominal[4] = 0.2;
    nominal[5] = 0.1;

    int decimal_multiplier = 100;


    int cash[6];
    int dummy_cash[6];
    float sum_of_money;
    float needed_money;
    string new_wallet;

    int insert_cash[6];

    float sum;
    float change_sum;
    int change[6];

    //PRODUCTS CONVERTERS
    int num_of_products;
    string full_stock = "10";
    string out_of_stock = "out of stock";
    string prd_am;
    int max_space = 10;

    int get_num, quant;
    int secret_code_1 = 1111;
    int secret_code_2 = 1112;

    int product_amount;

    //VALIDATION
    int success;

    //ARRAY OPERATORS
    int k;
    string current;
    int row;
    int col;

    string new_cash;
    string new_products;

    //--------------------------------------------

    //CASH ARRAY 
    data = access_file("../cash.txt");

    //CONVERT TO CASH ARRAY      
    n =  data.size(); //number of individual chars in a optained data 
    
    k = 0;
    for (int i = 0; i<n; i++){
        current =  data[i];
        if (current == next_r) k++;
        else cash_s[k] += current;
    }
    
    //convert cash to int array 
    for (int i = 0; i<6; i++){
        cash[i] = stoi(cash_s[i]);
        dummy_cash[i] = cash[i];
        sum_of_money += cash[i]*nominal[i];
    }
    

    //-----------------------------------------------------------
    //GET PRODUCTS ARRAY 
    data = access_file("../products.txt");
    n = data.size();

    num_of_products = number_of_products(data, next_r);
    string products[num_of_products][4];

    row = 0;
    col = 0;

    //MAKE PRODUCTS ARRAY 
    for (int i = 0; i < n; i++){
        current = data[i];
        if (current == next_c) col++;
        else if (current == next_r) {row++;col = 0;}
        else products[row][col] += current;
    }
    
    //FILL UP THE MACHINE AT THE START OF THE PROGRAM
    for (int i = 0; i <= row ; i++){
        if (products[i][2] == out_of_stock) products[i][2] = full_stock;
    }

    //START THE VENDING MACHINE 
    while(1){
        cout << "\n============================================================\n";
        display_products(row, col, products, max_space);

        //get customer request 
        cout << "Which product you want to pick?";
        cout << "(Enter the product number): ";
        cin >> get_num;

        success = validate_check(secret_code_1,secret_code_2,get_num,num_of_products);
        if (success== 1) continue;
        else if (success == 2) break; //secret code #1 detection 
        else if (success == 3) {money_edit(nominal, cash); continue;;} //secret code #2 detection 

        cout << "Pick quantity: ";
        cin >> quant;

        //check if product is avaliable 
        prd_am = products[get_num-1][2];
        if (prd_am == "out of stock") {cout << "Product out of stock! Please pick something else...\n"; continue;};

        product_amount = atoi(prd_am.c_str());
        //check if requested quantity is avaliable 
        if (product_amount - quant < 0){
            cout << "Requested quantity is not avaliable!\n";
            cout << "We can offer only: " << product_amount << " " << products[get_num-1][1];
            if (product_amount == 1) cout << "s";
            cout << ".\n";
            if (continue_check()) continue;
            quant = product_amount;
        }

        //total cost
        needed_money = quant*stof(products[get_num-1][3]);
        cout << "Final price: " << needed_money << " PLN\n";
        if (continue_check()) continue;

        sum = 0;
        empty_collector (insert_cash);

        //PAYMENT
        while(1){
            //COLLECT MONEY 
            while (round(needed_money*decimal_multiplier - sum*decimal_multiplier) > 0){
                payment(insert_cash, nominal);
                sum += coin_counter(insert_cash, nominal);
                cout << "You have inserted " << sum << " PLN\n";
                if (round(needed_money*decimal_multiplier - sum*decimal_multiplier) > 0) {
                    cout << "It is not enough! Please add " << needed_money - sum << " PLN more.\n";
                    if (continue_check()) continue;
                }
                //collect_coins(insert_cash, cash);
            }

            //CALCUALTE CHANGE 
            change_sum = round(sum*decimal_multiplier - needed_money*decimal_multiplier)/decimal_multiplier;
            cout << "Change: " << change_sum << " PLN\n";

            if (!calculate_change(change_sum, nominal, cash, insert_cash, change, decimal_multiplier)){
                cout << "Unfortunetely, the machine cannot give out change! \n";
                cout << "Please insert calculated amount of coins: " << needed_money << " PLN\n";
                if (continue_check()) continue;
                sum = 0;
                empty_collector (insert_cash);
            }else break;
        }
        //DISPLAY CHANGE 
        cout << "Thank you for the transaction.\nDon't forget your order!\n";
        if (round(change_sum*decimal_multiplier) != 0) display_change(change, change_sum, nominal);

        //UPTADE PRODUCT LIST 
        //update quantities
            if (product_amount - quant <= 0) products[get_num-1][2] = out_of_stock;
            else products[get_num-1][2] = to_string(product_amount - quant);
    }
    cout << "Exit\n";

    new_cash = cash_string(cash);
    new_products = product_string(num_of_products, 3, products);

    update_file("../cash.txt",new_cash);
    update_file("../products.txt",new_products);




    return 0;
}
//==================================================================================





string access_file(string adress){

    fstream file;
    file.open(adress, ios::in | ios::out);
    //if (file.good()) cout << "access cash allowed!\n";
    //else cout << "access cash denied!\n";

    //get string 
    string data;
    getline(file ,data);
    file.close();

    return data;
}

int calculate_change(float needed_money, float nominal[6], int *cash, int *insert_cash, int *change, int decimal_multiplier){
    
    int dummy_wallet[6];
    for(int i = 0; i<6; i++){
        dummy_wallet[i] = cash[i] + insert_cash[i];
    }
  
    int success = 1;
    int k = 0;
    needed_money *= decimal_multiplier;
    needed_money = round(needed_money);
    
    while (1){
        if (needed_money == 0) break;
        if ((round(needed_money - nominal[k]*decimal_multiplier) == 0) & (dummy_wallet[k] - 1 >= 0)) {dummy_wallet[k]-- ;break;}
        else if (needed_money - nominal[k]*decimal_multiplier < 0) k++;
        else{
            dummy_wallet[k]--;
            if (dummy_wallet[k] >= 0) {needed_money -= nominal[k]*decimal_multiplier; round(needed_money);}
            else {dummy_wallet[k]++; k++;}
        }
        if (k == 6){success = 0; break;}
    }
    
    if (success){
        for (int i = 0; i<6; i++){ 
            change[i] = cash[i] + insert_cash[i] - dummy_wallet[i];
            cash[i] = dummy_wallet[i]; 
        }
    }

    return success;
}

void display_change(int change[6], float change_sum, float nominal[6]){
    cout << "You recieved " << change_sum << " PLN in following amount of coins:\n";
    display_cash(change,nominal);
}
void display_cash(int cash[6], float nominal[6]){
    for (int i = 0; i<6; i++){
        cout << cash[i] << " x " << nominal[i] << " PLN\n";
    }
}

string new_cash_record(string next_r, int cash[6]){
    string new_wallet;
    for (int i = 0; i<6; i++){
        new_wallet += to_string(cash[i]);
        new_wallet += next_r;
    }
    return new_wallet;
}

int number_of_products(string data, string next_r){
    int k = data.size();
    k--;
    string current, previous;
    while(1){
        current = data[k-- -1];
        if (current == next_r) break;
        previous = current;
        if (k < 0) break;
    }
    int num_of_products = atoi(previous.c_str());
    return num_of_products;
}

void display_products(int row, int col, string products[][4], int max_space){
    int len;
    for (int i = 0; i <= row; i++){
        for (int j = 0; j <= col; j++){
            if (j == 2) cout << "remaining quantity: ";
            cout << products[i][j] << " ";
            if (j == 1){
                len = products[i][j].size();
                for (int k = 0; k <= max_space-len; k++) cout << " ";
            }
            if (j == 2){
                len = products[i][j].size();
                for (int k = 0; k <= 13-len; k++) cout << " ";
            }
            if (j == col){cout << " PLN";}
        }
        cout << "\n";
    }
}

int validate_check(int secret_code_1, int secret_code_2, int get_num, int num_of_products){
    int valid = 0;
    if (num_of_products < get_num) valid = 1;
    if (secret_code_1 == get_num) valid = 2;
    if (secret_code_2 == get_num) valid = 3;
    return valid;
}

int continue_check(void){
    int check = 1;
    string yes_check;
    cout << "Do you want to continue? If yes, type 'y'\n";
    cin >> yes_check;
    if (yes_check == "y") check = 0;
    return check;
}

void payment(int *insert_cash, float nominal[6]){
    int get_coin;
    cout << "Please enter number of coins you want to insert\n";
    for (int i = 0; i<6; i++){
        cout << "Number of " << nominal[i] << " PLN coins: ";
        cin >> get_coin;
        insert_cash[i] += get_coin;
    }
}

void empty_collector (int *insert_cash){
    for (int i = 0; i<6; i++) { insert_cash[i] = 0; }
}

void collect_coins(int *insert_cash, int  *cash){
    for (int i = 0; i<6; i++){
        cash[i] += insert_cash[i]; 
    }
}

float coin_counter(int *insert_cash, float nominal[6]){
    float sum;
    for (int i = 0; i<6; i++){ sum += insert_cash[i]*nominal[i]; }
    return sum;
}

void money_edit(float nominal[6], int *cash){
    string select;
    int selection;
    int number;

    int go = 1;
    while(go){
        cout << "\n============================================================\n";
        display_cash(cash,nominal);
        cout << "Which nominal do you want to edit?\n";
        for (int i = 0; i<6; i++){
            cout << "Type '" << i + 1 << "' to select " << nominal[i] << "\n";
        }
        cout << "Type '" << 7 << "' to exit edit\n";
        cin >> select;
        selection = stoi(select);
        
        if ((selection > 0) & (selection < 7)) cout << "How much of " << nominal[selection-1] << " will be in a system?\n";

        switch (selection) {
            case 1:
                cin >> number;
                break;
            case 2:
                cin >> number;
                break;
            case 3:
                cin >> number;
                break;
            case 4:
                cin >> number;
                break;
            case 5:
                cin >> number;
                break;
            case 6:
                cin >> number;
                break;
            case 7:
                go = 0;
                break;
            default: 
                cout << "Incorrect selection!";
                break;
        }
        if (number >= 0) cash[selection-1] = number;
        else cout << "Invalid amount!";
    }
}

void update_file(string address, string text){
    fstream file;
    file.open(address, ios::trunc | ios::out);
    //if (file.good()) cout << "access cash allowed!\n";
    //else cout << "access cash denied!\n";

    file << text;
    file.close();
}

string product_string(int row, int col, string products[][4]){
    string next_r = "|";
    string next_c = ",";

    string new_prod;

    for (int i = 0; i<row; i++){
        for (int j = 0; j<=col; j++){
            new_prod += products[i][j];
            if (j < col) new_prod += next_c;
        }
        if (i < row - 1) new_prod += next_r;
    }

    return new_prod;
}

string cash_string(int cash[6]){
    string next_r = "|";

    string new_prod;

    for (int i = 0; i<6; i++){
            new_prod += to_string(cash[i]) + next_r;
    }
    return new_prod;
}

