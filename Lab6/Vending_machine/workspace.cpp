#include <iostream>

void modify_array(int* arr, int size) {
  for (int i = 0; i < size; i++) {
    arr[i] = arr[i] * 2;  // Double each element
  }
}

int main() {
  int my_array[] = {1, 2, 3};
  int arr_size = sizeof(my_array) / sizeof(my_array[0]);
  std::cout << "Size of an array: " << arr_size << std::endl;

  std::cout << "Original array: ";
  for (int i = 0; i < arr_size; i++) {
    std::cout << my_array[i] << " ";
  }
  std::cout << std::endl;

  modify_array(my_array, arr_size);

  std::cout << "Modified array: ";
  for (int i = 0; i < arr_size; i++) {
    std::cout << my_array[i] << " ";
  }
  std::cout << std::endl;

  return 0;
}
