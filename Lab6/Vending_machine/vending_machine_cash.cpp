#include<iostream>
#include<string>
#include<fstream>
#include <cstdlib>

using namespace std; 

string access_file();
int calculate_change(float sum_of_money, float needed_money, float nominal[6], int *wallet);
void display_change(int dummy_cash[6] ,int cash[6], float needed_money, float nominal[6], int decimal_multiplier);
string new_cash_record(string next_r, int cash[6]);



int main(){
    //--PARAMETERS-DEFINITION---------------------
    
    //conversion characters
    string next_c = ".";
    string next_r = "|";

    //get file 
    string data;
    int n;

    //CASH CONVERTERS
    string cash_s[6]; //each space for current number of specific nominal monets: 5pln 2pln 1pln 0.5pln 0.2pln 0.1pln
    float nominal[6];

    nominal[0] = 5;
    nominal[1] = 2;
    nominal[2] = 1;
    nominal[3] = 0.5;
    nominal[4] = 0.2;
    nominal[5] = 0.1;

    int decimal_multiplier = 100;

    int cash[6];
    int dummy_cash[6];
    float sum_of_money;
    float needed_money;
    string new_wallet;

    //VALIDATION
    int success;

    //ARRAY OPERATORS
    int k;
    string current;

    //--------------------------------------------


    

    //CASH ARRAY 
    data = access_file("../cash.txt");

    //CONVERT TO CASH ARRAY      
    n =  data.size(); //number of individual chars in a optained data 
    
    for (int i = 0; i<6 ;i++){
        nominal[i] *= decimal_multiplier;
    }

    k = 0;
    for (int i = 0; i<n; i++){
        current =  data[i];
        if (current == next_r) k++;
        else cash_s[k] += current;
    }
    
    //convert cash to int array 
    for (int i = 0; i<6; i++){
        cash[i] = stoi(cash_s[i]);
        dummy_cash[i] = cash[i];
        //cout << cash[i] << endl;
        sum_of_money += cash[i]*nominal[i];
    }
    //cout << "Currently we machine has " << sum_of_money/decimal_multiplier << " PLN" << endl;









    //cout << "How much money you need to get?: ";
    cin >> needed_money;

    sum_of_money *= decimal_multiplier;
    needed_money *= decimal_multiplier;
    
    success = calculate_change(sum_of_money, needed_money, nominal, cash);

    if (success){
        //display the given coins 
        display_change(dummy_cash, cash, needed_money, nominal, decimal_multiplier);
    }else cout << "We are not able to give such value to you :< Sorry" << endl;

    //update current wallet
    new_wallet = new_cash_record(next_r, cash);
    cout << new_wallet << endl;

    return 0;
}

string access_file(string adress){

    fstream file;
    file.open(adress, ios::in | ios::out);
    if (file.good()) cout << "access cash allowed!" << endl;
    else cout << "access cash denied!" << endl;

    //get string 
    string data;
    getline(file ,data);
    file.close();

    return data;
}

int calculate_change(float sum_of_money, float needed_money, float nominal[6], int *cash){
    int dummy_wallet[6];
    for(int i = 0; i<6; i++){
        dummy_wallet[i] = cash[i];
    }

    int success = 1;
    int k = 0;
    if (sum_of_money - needed_money < 0) {cout << "There is no enough cash in the machine :< " << endl; success = 0;}
    else{
        while (1){
            //cout << "Current amout of needed money: " << needed_money/100 << endl;
            if (needed_money == 0) {cout << "Here is your change" << endl; break;}
            if ((needed_money - nominal[k] == 0) & (dummy_wallet[k] - 1 >= 0)) {cout << "Here is your change" << endl; dummy_wallet[k]-- ;break;}
            else if (needed_money - nominal[k] < 0) k++;
            else{
                dummy_wallet[k]--;
                if (dummy_wallet[k] >= 0) needed_money -= nominal[k];
                else {dummy_wallet[k]++; k++;}
            }
            if (k == 6){success = 0; break;}
        }
    }
    if (success){ for (int i = 0; i<6 ;i++){ cash[i] = dummy_wallet[i]; } }

    return success;
}

void display_change(int dummy_cash[6] ,int cash[6], float needed_money, float nominal[6], int decimal_multiplier){
    cout << "You recieved " << needed_money/decimal_multiplier << " PLN in following amount of coins:" << endl;
    for (int i = 0; i<6; i++){
        cout << dummy_cash[i] - cash[i] << " x " << nominal[i]/decimal_multiplier << " PLN" << endl;
    }
}

string new_cash_record(string next_r, int cash[6]){
    string new_wallet;
    for (int i = 0; i<6; i++){
        new_wallet += to_string(cash[i]);
        new_wallet += next_r;
    }
    return new_wallet;
}