#include "class.h"
#include<iostream>
#include<string>
using namespace std;


int Product::total = 0;

void Product::set_product(int num, string nam, float pri, int qua){
        number = num;
        name = nam;
        price = pri;
        quant = qua;
        if (quant <= 0) refill_machine();
    }
    
void Product::display(){
    float price_f = price; 
    cout << number << ". " << name;
    for (int k = 0; k <= 10-name.size(); k++) cout << " ";
    cout << " price: " << price_f/100 << " PLN q. " << quant;
    if (quant <= 0) cout << " -- out of stock!";
    cout << "\n";
}

void Product::change_quant(int get_value){
    quant -= get_value;
}

void wallet::insert_cash(int no_500, int no_200, int no_100, int no_50, int no_20, int no_10){
    quant[0] += no_500;
    quant[1] += no_200;
    quant[2] += no_100;
    quant[3] += no_50;
    quant[4] += no_20;
    quant[5] += no_10;
}

void wallet::insert_cash(int q[6]){
    for (int i = 0; i<6; i++){
        quant[i] += q[i];
    }
}

int wallet::get_sum(){
    int sum = 0;
    for (int i = 0; i<=5; i++){
        sum += value[i]*quant[i];
    }
    return sum;
 }

int wallet::calculate_change(int needed_money, wallet &change){
    int success = 1, current_nominal = 0;
    
    //clone the original wallet 
    wallet dummy_wallet; 
    dummy_wallet.insert_cash(quant);

    while(1){
        if (needed_money == 0) break;
        if ((needed_money - value[current_nominal] == 0) & (quant[current_nominal] - 1 >= 0)) {dummy_wallet.quant[current_nominal]--; break;}
        else if (needed_money - value[current_nominal] < 0) current_nominal++;
        else{
            dummy_wallet.quant[current_nominal]--;
            if (dummy_wallet.quant[current_nominal] >= 0) needed_money -= value[current_nominal];
            else {dummy_wallet.quant[current_nominal]++; break;}
        }
    }

    if (success){
        change.add_wallet_content(quant, 1);
        change.add_wallet_content(dummy_wallet, -1);
        add_wallet_content(change, -1);
    }
    return success;
}
/*
void wallet::add_wallet_content(wallet old_wallet, int mode = 1){
    for (int i = 0; i<6; i++){
        quant[i] += mode*old_wallet.quant[i];
    }
}

void wallet::add_wallet_content(int old_wallet[6], int mode = 1){
    for (int i = 0; i<6; i++){
        quant[i] += mode*old_wallet[i];
    }
}
*/
void wallet::display(){
    for(int i = 0; i<6; i++){
        cout << quant[i] << " x " << nominal[i] << " PLN\n";
    }
}

void wallet::reset(){
    quant[0] = 0;
    quant[1] = 0;
    quant[2] = 0;
    quant[3] = 0;
    quant[4] = 0;
    quant[5] = 0;
}
