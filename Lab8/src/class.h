#include<iostream>
#ifndef class_H
#define class_H


class Product{
    public:
        int number;
        std::string name;
        int price; 
        int quant;

        static int total;

        void set_product(int num, std::string nam, float pri, int qua);
        void display();
        void change_quant(int get_value);


    private:
        const int refill = 10;
        void refill_machine(){
            quant = refill;
        }
    
};

class wallet{
    public:
        int quant[6] = {0,0,0,0,0,0};
        float nominal[6];
        int value[6];
        
        void insert_cash(int no_500, int no_200, int no_100, int no_50, int no_20, int no_10);
        void insert_cash(int q[6]);
        int get_sum();
        int calculate_change(int needed_money, wallet &change);
        void add_wallet_content(wallet old_wallet, int mode);
        void add_wallet_content(int old_wallet[6], int mode);
        void display();
        void reset();

        wallet(){
            nominal[0] = 5;
            nominal[1] = 2;
            nominal[2] = 1;
            nominal[3] = 0.5;
            nominal[4] = 0.2;
            nominal[5] = 0.1;
            
            value[0] = 500;
            value[1] = 200;
            value[2] = 100;
            value[3] = 50;
            value[4] = 20;
            value[5] = 10;
        }

        ~wallet(){
            //cout << "Wallet deleted\n";
        }

};



#endif