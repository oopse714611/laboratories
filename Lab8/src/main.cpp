#include<iostream>
#include<fstream>
#include"class.h"

using namespace std;

void wallet::add_wallet_content(wallet old_wallet, int mode = 1){
    for (int i = 0; i<6; i++){
        quant[i] += mode*old_wallet.quant[i];
    }
}

void wallet::add_wallet_content(int old_wallet[6], int mode = 1){
    for (int i = 0; i<6; i++){
        quant[i] += mode*old_wallet[i];
    }
}


void get_total_number();
void get_products(Product &product, int index);
void get_money(wallet &wallet);

int validity(int input);
int validity_q(int quant, Product prod);
int continue_question();

int calculate_cost(int quant, int price);
void payment(wallet &insert_wallet);
void money_edit(wallet &money);
int update_money_file(wallet money);
//=========================================================
//=========================================================
//=========================================================

int main(){
    //construct vending machine 
    //get products 
    get_total_number();
    Product product[Product::total];
    for (int i = 0; i<Product::total; i++){
        get_products(product[i],i);
    }

    //make wallet 
    wallet machine_cash;
    get_money(machine_cash);

    int get_num;
    int check;
    int quant;
    string answer;
    int needed_money;

    while (1){
        cout << "================================================\n";
        cout << "=================Vending_Machine================\n";
        cout << "================================================\n";
        //OPERATION ===============
        //display products
        for (int i = 0; i<Product::total; i++){
            product[i].display();
        }
        //user input 
        cout << "Which product you want to pick?";
        cout << "(Enter the product number): ";
        cin >> get_num;
        //check codes and validity 
        check = validity(get_num);
        if (check == 1) break;
        if (check == 2) {money_edit(machine_cash); continue;}
        if (check == 0) {cout << "Wrong number!\n"; continue;}

        cout << "Pick quantity: ";
        cin >> quant;

        check = validity_q(quant, product[get_num - 1]);
        if (check == -1){cout << "Invalid input!\n"; continue;}
        if (check == 0) {cout << "Out of stock!\n";continue;}
        else if (check < quant){
            cout << "Only " << check << " of " << product[get_num - 1].name << " left!\n";
            if (!continue_question()) continue;
            quant = check;
        }
        else if (check == quant) quant = check;
        else {cout << "error!"; return 1;}

        //PAYMENT ==================================
        //needed money
        needed_money = calculate_cost(quant, product[get_num - 1].price);
        cout << "Total cost: " << float(needed_money) / 100 << " PLN\n";
        //user input - money 
        check = 1;
        wallet insert_wallet;
        wallet change;
        wallet dummy_wallet;

        while (check == 1){
            payment(insert_wallet);
            if (insert_wallet.get_sum() < needed_money) {
                cout << "Please insert " << float(needed_money - insert_wallet.get_sum())/100 << " PLN more\n";
                if (continue_question()) continue;
                else break;
            }
            cout << "You have inserted " << float(insert_wallet.get_sum())/100 << " PLN\n";
            //calculate change 

            machine_cash.add_wallet_content(insert_wallet);
            if (machine_cash.calculate_change(insert_wallet.get_sum() - needed_money,change)){
                cout << "Your change of " << float(insert_wallet.get_sum() - needed_money)/100 << " PLN:\n";
                change.display();
                check = 2;
                break;
            }else{
                machine_cash.add_wallet_content(insert_wallet, -1);
            }
        }
        if (check == 2){
            //change product quantity 
            product[get_num-1].change_quant(quant);
        }
    }
    cout << "Thank you\n";
    cout << "Exit\n";


    //==================
    //File update 
    update_money_file(machine_cash);
    fstream file2;
    file2.open("./external/products.txt", ios::trunc | ios::out);
    if(file2.good()){
        for (int i = 0; i<Product::total; i++){
            file2 << product[i].number << " " << product[i].name << " " << product[i].price<< " " << product[i].quant << "\n";
        }
    }else{cout << "Error!!!";}
    file2.close();

    return 0;
}

//=========================================================
//=========================================================
//=========================================================

void get_total_number(){
    //check how many correct products sets are there
    fstream file;
    file.open("products.txt");
    if (file.fail()) cout << "Fill access error.";

    int number;
    string name;
    float price;
    int quant;

    int total = 0;

    while (file >> number >> name >> price >> quant) {total++;}

    file.close();
    Product::total = total;
}

void get_products(Product &product, int index){
    fstream file;
    file.open("./external/products.txt");
    if (file.fail()) cout << "Fill access error.";

    int number;
    string name;
    float price;
    int quant;
    
    for (int i = 0; i <= index; i++){
        file >> number >> name >> price >> quant;
    }
    product.set_product(number,name,price,quant);

    file.close();
}

void get_money(wallet &wallet){
    fstream file;
    file.open("./external/cash.txt");
    if (file.fail()) cout << "Fill access error.";

    int no_500, no_200, no_100, no_50, no_20, no_10;

    file >> no_500 >> no_200 >> no_100 >> no_50 >> no_20 >> no_10;
    wallet.insert_cash(no_500, no_200, no_100, no_50, no_20, no_10);
}

int validity(int input){
    int code_1 = 1111; //exit
    int code_2 = 1112; //money edit 

    if (input == code_1) return 1;
    if (input == code_2) return 2;
    if (input > 0 & input <= Product::total) return 3;
    return 0;
    }

int validity_q(int quant, Product prod){
    if (quant == 0) return -1;
    if (prod.quant - quant < 0) return prod.quant;
    return quant;
}

int calculate_cost(int quant, int price){
    return quant*price;
}

void payment(wallet &insert_wallet){
    int get_coin[6];

    cout << "Please enter number of coins you want to insert:\n";
    for (int i = 0; i<6; i++){
        cout << "Number of " << insert_wallet.nominal[i] << " PLN coins: ";
        cin >> get_coin[i];
    }
    insert_wallet.add_wallet_content(get_coin,1);
}

int continue_question(){
    int success = 2;
    string answer;
    while (success == 2){
        cout << "Do you want to continue? [y/n]: ";
        cin >> answer;
        if (answer == "y") success = 1;
        else if (answer == "n") success = 0;
        else cout << "Wrong answer!\n";
    }
    return success;
}

void money_edit(wallet &money){
    int answer;
    int quant;

    cout << "===============================\n";
    cout << "Now you can edit machine money!\n";
    while (1){

        cout << "Which nominal you want to edit?\n";
        for (int i = 0; i<6; i++){
            cout << i << ". " << money.nominal[i] << " PLN currently: " << money.quant[i] << " x " << money.nominal[i] << " PLN\n";
        }
        cout << "6. exit\n";
        cout << "Choose the nominal number:";
        cin >> answer;
        if (answer < 0 || answer > 6) continue;
        if (answer == 6) break;

        cout << "Select new quantity: ";
        cin >> quant;

        money.quant[answer] = quant;
    }
    cout << "Money edit finished\n";
}

int update_money_file(wallet money){    
    int success = 1;
    string address = "./cash.txt";
    fstream file;
    file.open(address, ios::out | ios::trunc);
    if (file.fail()) success = 0;

    for (int i = 0; i<6; i++){
        file << money.quant[i] << " ";
    }

    file.close();

    return success;
}
/*
int update_product_file(Product prod, int i){
    int success = 1;
    string address = "./external/products.txt";
    fstream file;
    file.open(address, ios::out | ios::trunc);
    if (file.fail()) success = 0;
        
    for (int i = 0; i<Product::total; i++){
        //file << i << " " << prod.name[i] << " " << prod.price[i] << " " << prod.quant[i] << "\n";
    }
    file.close();
    return success;
}
*/