#include"functions.hpp"

#include<iostream>
#include<limits>

using namespace std;

void dispMenu(){
    cout << "What do you want to do?\n";
    cout << "1. Load hero.\n";
    cout << "2. Make new hero.\n";
    cout << "3. Save hero.\n";
    cout << "4. Edit hero.\n";
    cout << "5. Exit game.\n";
}

int user_action(){
    dispMenu();
    return get_num_answer(1,5);
}

int get_num_answer(int min, int max){
    int number;

    while (true){
        cout << "Choose number between " << min << " and " << max << ": ";
        if (cin >> number){
            if (number >= min && number <= max){
                return number;
            }else{
                cout << "Wront input\n";
                continue;
            }
        }else{
            cout << "Wrong input!\n";
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            continue;
        }
    }

    return number;
}