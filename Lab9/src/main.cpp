#include<iostream>
#include<string>
#include<fstream>
#include <limits>

using namespace std;

#include"class.hpp"
#include"functions.hpp"

int main(){
    character hero;
    
    string answer;
    int exit = 1;

    //the game 
    while(exit){
        hero.display_full();
        switch(user_action()){
        case 1:
            //Load hero
            cout << "What is your hero's name? : ";
            cin >> answer;
            hero.load(answer);
            break;
        case 2:
            //Create a new hero
            hero.set_to_default();
            cout << "New neme: ";
            cin >> answer;
            hero.change_name(answer);
            hero.edit();
            break;
        case 3:
            //Save hero
            hero.save_character();
            cout << "Your hero has been saved\n";
            break;
        case 4: 
            //Edit hero
            hero.edit();
            break;
        case 5:
            //Exit
            cout << "Do you want to save your hero? [y/n]: ";
            if (cin >> answer){
                exit = 0;
                if (answer == "y") {hero.save_character(); cout << "Thank you for playing. Your hero has been saved.\n";}
                else if (answer == "n") cout << "Thank you for playing. Your hero has not been saved!\n";
                else {cout << "No exit for you! Try one more time.\n"; exit = 1;}
            }
            break;
        default:
            cout << "Error! Try other option.\n";
            break;
        } 
    }


    return 0;
}