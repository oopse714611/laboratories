#include"class.hpp"
#include"functions.hpp"

#include<iostream>
#include<string>
#include<fstream>

using namespace std;

character::character(){
    set_to_default();
}

character::character(std::string Name){
    set_to_default();
    change_name(Name);
}

character::character(std::string Name, int Strength, int Dexterity, int Endurance, int Intelligence, int Charisma){
    name = Name;
    strength = Strength;
    dexterity = Dexterity;
    endurance = Endurance;
    intelligence = Intelligence;
    charisma = Charisma;
}

void character::set_to_default(){
    name = "Unknown";
    strength = 5;
    dexterity = 5;
    endurance = 5;
    intelligence = 5;
    charisma = 5;
}

void character::edit(){
    cout << "Strength\n";
    edit_Strength(get_num_answer(0, 10));
    cout << "Dexterity\n";
    edit_Dexterity(get_num_answer(0, 10));
    cout << "Endurance\n";
    edit_Endurance(get_num_answer(0, 10));
    cout << "Intelligence\n";
    edit_Intelligence(get_num_answer(0, 10));
    cout << "Charisma\n";
    edit_Charisma(get_num_answer(0, 10));
}

void character::change_name(std::string new_name){name = new_name;}
void character::edit_Strength(int v){strength = v;}
void character::edit_Dexterity(int v){dexterity = v;}
void character::edit_Endurance(int v){endurance = v;}
void character::edit_Intelligence(int v){intelligence = v;}
void character::edit_Charisma(int v){charisma = v;}


void character::load(std::string address){
    if (address != "Unknown"){
        std::string full_address = "./" + address + ".txt";
        fstream file;
        file.open(full_address, ios::in);
        if (file.fail()) cout << "Error during openning file: " << "address\n";
        else{
            file >> name >> strength >> dexterity >> endurance >> intelligence >> charisma;
        }
        file.close();
    }else cout << "Cannot save default heros!\n";
}


void character::display_full(){
    cout << "======================";
    cout << "\nName: " << name << "\nStrength: " << strength << "\nDexterity: " << dexterity << "\nEndurance: " << endurance << "\nIntelligence: " << intelligence << "\nCharisma: " << charisma; 
    cout << "\n======================\n";
}

void character::save_character(){
    if (name != "Unknown"){
        string full_address = "./" + name + ".txt";
        fstream file;
        file.open(full_address, ios::out | ios::trunc);
        if (file.fail()) cout << "Error saving the character\n";
        else{
            file << name << " " << strength << " " << dexterity << " " << endurance << " " << intelligence << " " << charisma;
            cout << "Character saved.\n";
        }
        file.close();
    }else cout << "Cannot save default heroes!\n";
}