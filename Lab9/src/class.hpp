#include<iostream>
#include<string>
#ifndef class_HPP
#define class_HPP

class character{
    private:
        int default_value = 5;
        int strength;
        int dexterity;
        int endurance;
        int intelligence;
        int charisma; 
    public:
        std::string name;
        void load(std::string address);
        void change_name(std::string new_name);
        void display_full();
        void save_character();
        void set_to_default();
        void edit_Strength(int v);
        void edit_Dexterity(int v);
        void edit_Endurance(int v);
        void edit_Intelligence(int v);
        void edit_Charisma(int v);
        void edit();
        character();
        character(std::string Name);
        character(std::string Name, int Strength, int Dexterity, int Endurance, int Intelligence, int Charisma);
};



#endif