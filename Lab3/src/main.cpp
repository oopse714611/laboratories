#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;

int number_of_products(string data, string next_l);
int validate_check(int secret_code,int get_num, int num_of_products);
int quant_check(int prod,int quant);
void display_products(int row, int col, string products[][3], int max_space);
string product_string(int row, int col, string products[][3], string next_s, string next_l);


int main(){
    
    fstream file;
    file.open("./test.txt", ios::in);
    //if (file.good()) cout << "access cash allowed!\n";
    //else cout << "access cash denied!\n";
    
    string data;
    getline(file ,data);
    file.close();

    
    int n = data.size();
    
    //cout << n << endl;
    //cout << data << endl;

    string next_c = ".";
    string next_r = "|";

    string out = "out of stock";
    string prd_am;

    
    //int n = data.size();

    int num_of_products = number_of_products(data, next_r);

    string products[num_of_products][3];

    string current;
    int row = 0;
    int col = 0;

    for (int i = 0; i < n; i++){
        current = data[i];
        if (current == next_c) col++;
        else if (current == next_r) {row++;col = 0;}
        else products[row][col] += current;
    }

    string full_stock = "10";

    for (int i = 0; i <= row ; i++){
        if (products[i][2] == "out of stock") products[i][2] = full_stock;
    }



    int max_space = 10;
    //display_products(row, col, products, max_space);



    int get_num, quant;
    int secret_code = 1111;
    char buffer[100];
    string buf;
    while (1){

        display_products(row, col, products, max_space);


        cout << "Which product you want to pick?";
        cout << "(Enter the product number): ";
        cin >> get_num;

        if (validate_check(secret_code,get_num,num_of_products) == 1) continue;
        else if (validate_check(secret_code,get_num,num_of_products) == 2) break;

        cout << "Pick quantity: ";
        cin >> quant;

        prd_am = products[get_num-1][2];
        if (prd_am == "out of stock") cout << "Product out of stock! Please pick something else..." << endl;
        else{
            int product_amount = atoi(prd_am.c_str());
            if (product_amount - quant < 0) cout << "You got " << product_amount << " of " << products[get_num-1][1] << endl;
            else cout << "You got " << quant << " of " <<   products[get_num-1][1] << endl;

            //update quantities
            if (product_amount - quant <= 0) products[get_num-1][2] = out;
            else products[get_num-1][2] = to_string(product_amount - quant);
        }
    }

    cout << "\nExit" << endl;

    string new_produsct_string = product_string(row, col, products, next_c, next_r);


    //cout << new_produsct_string << endl;




    file.open("./test.txt", ios::out | ios::trunc);
    //if(file.good()) cout << "File accessed" << endl;
    //else cout << "access denied" << endl;
    file << new_produsct_string;
    file.close();



    //cout << "Exit" << endl;
    return 0;
}


int number_of_products(string data, string next_r){
    int k = data.size();
    string current, previous;
    while(1){
        current = data[k-- -1];
        if (current == next_r) break;
        previous = current;
        if (k < 0) break;
    }
    int num_of_products = atoi(previous.c_str());
    return num_of_products;
}


void display_products(int row, int col, string products[][3], int max_space){
    int len;
    for (int i = 0; i <= row; i++){
        for (int j = 0; j <= col; j++){
            cout << products[i][j] << " ";
            if (j == 1){
                len = products[i][j].size();
                for (int k = 0; k <= max_space-len; k++) cout << " ";
            }
        }
        cout << "\n";
    }
}


int validate_check(int secret_code,int get_num, int num_of_products){
    int valid = 0;
    if (num_of_products < get_num) valid = 1;
    if (secret_code == get_num) valid = 2;
    return valid;
}

int quant_check(int prod, int quant){
    return (prod - quant);
}

string product_string(int row, int col, string products[][3], string next_c, string next_r){
    string new_produsct_string;
    for (int i = 0; i <= row; i++){
        for (int j = 0; j <= col; j++){
            new_produsct_string += products[i][j];
            if (j < col) new_produsct_string += next_c;
        }
        if (i < row) new_produsct_string += next_r;
    }
    return new_produsct_string;
}
